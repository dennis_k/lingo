export function validate(guess: string[], solution: string[]): Array<'wrong' | 'close' | 'correct'> {
  const remainingChars: string[] = [];
  const firstPass = guess.map((guessChar, i) => {
    if (guessChar === solution[i]) {
      return 'correct';
    }
    remainingChars.push(solution[i]);
    return null;
  });
  const results = firstPass.map((firstClassification, i) => {
    if (firstClassification !== null) {
      // This character has already been classified:
      return firstClassification;
    }
    const inRemainingChars: number = remainingChars.indexOf(guess[i]);
    if (inRemainingChars !== -1) {
      delete remainingChars[inRemainingChars];
      return 'close';
    }
    return 'wrong';
  });

  return results;
}