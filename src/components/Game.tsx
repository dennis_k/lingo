import React from 'react';
import { Guess } from './Guess';
import { useGameState } from '../hooks/useGameState';
import { couldBeShortcut } from '../keyboardEvent';

export type Mode = 5 | 6 | 7 | 8;
const possibleModes: Mode[] = [5, 6, 7, 8];

interface Props {
  timeout?: number;
};

export const Game: React.FC<Props> = ({ timeout = -1 } = {}) => {
  const wrapperRef = React.useRef<HTMLDivElement>(null);
  const [game, actions] = useGameState({ timeout });
  React.useLayoutEffect(() => {
    if (game.solution && wrapperRef.current) {
      wrapperRef.current.focus();
    }
  }, [game.solution, wrapperRef]);

  React.useLayoutEffect(() => {
    window.scrollTo(0, document.body.scrollHeight);
  }, [game.currentGuess, game.guesses]);

  const solution = game.solution;
  if (!solution) {
    return <div>&hellip;</div>;
  }

  const guessElements = game.guesses.concat([game.currentGuess]).map((guess, i) => (
    <Guess
      key={`guess${i}`}
      guess={guess}
      solution={solution || ''}
      validate={i < game.guesses.length}
      foundLetters={game.foundLetters}
    />
  ));

  const handleKeyPress: React.KeyboardEventHandler = (event) => {
    // Do not override browser shortcuts:
    if (couldBeShortcut(event)) {
      return;
    }

    if ('Tab' === event.key) {
      event.preventDefault();
      actions.toggleFinale();
    }

    if (possibleModes.includes(Number.parseInt(event.key, 10) as Mode)) {
      event.preventDefault();
      actions.setMode(Number.parseInt(event.key, 10) as Mode);
      actions.loadNext();
    }
    if ('Escape' === event.key) {
      event.preventDefault();
      actions.addGuess(solution);
    }
    if ('Backspace' === event.key || /^[A-Za-z]$/.test(event.key)) {
      event.preventDefault();
      actions.addChar(event.key.toLowerCase());
    }
    if (event.key === 'Enter') {
      // If the word has been guessed correctly, start a new round when pressing Enter:
      if (game.guesses[game.guesses.length - 1] === solution) {
        actions.loadNext();
      } else if (game.currentGuess.length === solution.length && game.currentGuess.charAt(0) === solution.charAt(0)) {
        actions.addGuess(game.currentGuess);
        actions.addChar(null);
      }
    }
  };

  const relevantGuessElements = (game.guesses[game.guesses.length - 1] === solution)
    // The last element is the next guess, which we don't need to show if the word has been guessed:
    ? guessElements.slice(0, guessElements.length - 1)
    : guessElements;

  return (
    <div
      tabIndex={0}
      onKeyDown={handleKeyPress}
      ref={wrapperRef}
      className="game"
    >
      {relevantGuessElements}
    </div>
  );
};
