import React from 'react';

export function couldBeShortcut(event: React.KeyboardEvent): boolean {
  return event.ctrlKey || event.altKey || event.metaKey ||
    event.key === 'Control' || event.key === 'Alt' || event.key === 'Meta';
}